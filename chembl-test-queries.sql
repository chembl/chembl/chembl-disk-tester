--Check if any marked MOL structure in molecule_dictioanry has no entry in compound_structures
 SELECT molecule_dictionary.molregno FROM molecule_dictionary WHERE molecule_dictionary.structure_type = 'MOL' AND molecule_dictionary.molregno NOT IN (SELECT compound_structures.molregno FROM compound_structures);
​
--Check if any marked SEQ structure in molecule_dictionary has no entry in biotherapeutics
 SELECT molecule_dictionary.molregno FROM molecule_dictionary WHERE molecule_dictionary.structure_type = 'SEQ' AND molecule_dictionary.molregno NOT IN (SELECT biotherapeutics.molregno FROM biotherapeutics);
​
--Check that molecules with NONE structure type don't really have MOL or SEQ in compound_structures or biotherapeutics tables
 SELECT molecule_dictionary.molregno FROM molecule_dictionary WHERE molecule_dictionary.structure_type = 'NONE' AND molecule_dictionary.molregno IN (SELECT anon_1.biotherapeutics_molregno FROM (SELECT biotherapeutics.molregno AS biotherapeutics_molregno FROM biotherapeutics UNION SELECT compound_structures.molregno AS compound_structures_molregno FROM compound_structures) AS anon_1);
​
--We should not have any(not downgraded) molecule in molecule_dictionary that is not child, parent or active in molecule hierarchy
 SELECT anon_1.molecule_dictionary_molregno FROM (SELECT molecule_dictionary.molregno AS molecule_dictionary_molregno FROM molecule_dictionary EXCEPT SELECT molecule_hierarchy.molregno AS molecule_hierarchy_molregno FROM molecule_hierarchy EXCEPT SELECT molecule_hierarchy.parent_molregno AS molecule_hierarchy_parent_molregno FROM molecule_hierarchy EXCEPT SELECT molecule_hierarchy.active_molregno AS molecule_hierarchy_active_molregno FROM molecule_hierarchy) AS anon_1;
​
--The hierarchy has too many layers i.e., parent of a salt has been stripped again to a superparent
 SELECT molecule_hierarchy.parent_molregno FROM molecule_hierarchy WHERE molecule_hierarchy.parent_molregno != molecule_hierarchy.molregno AND molecule_hierarchy.parent_molregno IN (SELECT molecule_hierarchy.molregno FROM molecule_hierarchy WHERE molecule_hierarchy.parent_molregno != molecule_hierarchy.molregno);
​
--Molecules with structure_type = 'MOL' that have no compound_properties
 SELECT molecule_dictionary.molregno FROM molecule_dictionary WHERE molecule_dictionary.structure_type = 'MOL' AND molecule_dictionary.molregno NOT IN (SELECT compound_properties.molregno FROM compound_properties);
​
--Get molregnos with no chembl id
 SELECT molecule_dictionary.molregno FROM molecule_dictionary WHERE molecule_dictionary.chembl_id IS NULL;
​
--Compounds in molecule_dictionary that don't have a chembl_id in chembl_id_lookup
 SELECT molecule_dictionary.molregno, molecule_dictionary.pref_name, molecule_dictionary.chembl_id, molecule_dictionary.max_phase, molecule_dictionary.therapeutic_flag, molecule_dictionary.dosed_ingredient, molecule_dictionary.structure_type, molecule_dictionary.chebi_par_id, molecule_dictionary.molecule_type, molecule_dictionary.first_approval, molecule_dictionary.oral, molecule_dictionary.parenteral, molecule_dictionary.topical, molecule_dictionary.black_box_warning, molecule_dictionary.natural_product, molecule_dictionary.first_in_class, molecule_dictionary.chirality, molecule_dictionary.prodrug, molecule_dictionary.inorganic_flag, molecule_dictionary.usan_year, molecule_dictionary.availability_type, molecule_dictionary.usan_stem, molecule_dictionary.polymer_flag, molecule_dictionary.usan_substem, molecule_dictionary.usan_stem_definition, molecule_dictionary.indication_class, molecule_dictionary.withdrawn_flag, molecule_dictionary.withdrawn_year, molecule_dictionary.withdrawn_country, molecule_dictionary.withdrawn_reason, molecule_dictionary.withdrawn_class FROM molecule_dictionary WHERE molecule_dictionary.chembl_id NOT IN (SELECT chembl_id_lookup.chembl_id FROM chembl_id_lookup WHERE chembl_id_lookup.entity_type = 'COMPOUND');
​
--Molecules with no inchi_key
 SELECT compound_structures.molregno FROM compound_structures WHERE compound_structures.standard_inchi_key IS NULL;
​
--Molecules with no canonical_smiles
 SELECT compound_structures.molregno FROM compound_structures WHERE compound_structures.canonical_smiles IS NULL;
​
--Targets with protein parents that are not in target_components
 SELECT target_dictionary.tid FROM target_dictionary JOIN target_type ON target_dictionary.target_type = target_type.target_type WHERE target_type.parent_type = 'PROTEIN' AND target_dictionary.tid NOT IN (SELECT target_components.tid FROM target_components);
​
--Targets with ('NON-MOLECULAR','UNDEFINED') parent_type that are in target_components
 SELECT target_dictionary.tid FROM target_dictionary JOIN target_type ON target_dictionary.target_type = target_type.target_type WHERE target_type.parent_type IN ('NON-MOLECULAR', 'UNDEFINED') AND target_dictionary.tid IN (SELECT target_components.tid FROM target_components);
​
--Protein component sequence with any null field
 SELECT component_sequences.component_id FROM component_sequences WHERE component_sequences.component_type = 'PROTEIN' AND (component_sequences.accession IS NULL OR component_sequences.db_source IS NULL OR component_sequences.db_version IS NULL OR component_sequences.sequence IS NULL OR component_sequences.sequence_md5sum IS NULL OR component_sequences.description IS NULL OR component_sequences.organism IS NULL OR component_sequences.tax_id IS NULL);
​
--Target with null chembl_id
 SELECT target_dictionary.tid FROM target_dictionary WHERE target_dictionary.chembl_id IS NULL;
​
--Targets with no entry in chembl_id_lookup
 SELECT target_dictionary.tid FROM target_dictionary WHERE target_dictionary.chembl_id NOT IN (SELECT chembl_id_lookup.chembl_id AS chembl_id_lookup_chembl_id FROM chembl_id_lookup WHERE chembl_id_lookup.entity_type = 'TARGET');
​
--Targets with null target_type
 SELECT target_dictionary.tid FROM target_dictionary WHERE target_dictionary.target_type IS NULL;
​
--Assays with null tid
 SELECT assays.assay_id FROM assays WHERE assays.tid IS NULL;
​
--Assay with assay_tax_id = 0
 SELECT assays.assay_id FROM assays WHERE assays.assay_tax_id = 0;
​
--Assay with null(or) confidence_score/relationship_type/curated
 SELECT assays.assay_id FROM assays WHERE assays.confidence_score IS NULL OR assays.relationship_type IS NULL OR assays.curated_by IS NULL;
​
--Assays with null chembl_id
 SELECT assays.assay_id FROM assays WHERE assays.chembl_id IS NULL;
​
--Assays with no 'ASSAY' entry in chembl_id_lookup
 SELECT assays.assay_id FROM assays WHERE assays.chembl_id NOT IN (SELECT chembl_id_lookup.chembl_id FROM chembl_id_lookup WHERE chembl_id_lookup.entity_type = 'ASSAY');
​
--Activity with null(or) doc_id/molregno
 SELECT activities.activity_id FROM activities WHERE activities.doc_id IS NULL OR activities.molregno IS NULL;
​
--Something with assay_tax_id and tax_id in organism_class
 SELECT anon_2.anon_1_assays_assay_tax_id FROM (SELECT anon_1.assays_assay_tax_id AS anon_1_assays_assay_tax_id FROM (SELECT assays.assay_tax_id AS assays_assay_tax_id FROM assays WHERE assays.assay_tax_id IS NOT NULL UNION SELECT target_dictionary.tax_id AS target_dictionary_tax_id FROM target_dictionary WHERE target_dictionary.tax_id IS NOT NULL) AS anon_1 EXCEPT SELECT organism_class.tax_id AS organism_class_tax_id FROM organism_class) AS anon_2;
​
--Missmatched docs between assays and records
 SELECT ss.molregno FROM molecule_synonyms AS ss JOIN molecule_synonyms AS ss2 ON ss.molregno = ss2.molregno WHERE ss.synonyms != ss2.synonyms AND ss.syn_type = ss2.syn_type AND lower(ss.synonyms) = lower(ss2.synonyms);
​
--Docs with null doc_type
 SELECT docs.doc_id FROM docs WHERE docs.doc_type IS NULL;
​
--Docs('PUBLICATIONS') with null year or journal or (null pub_med_id and null doi)
 SELECT docs.doc_id, docs.journal, docs.year, docs.volume, docs.issue, docs.first_page, docs.last_page, docs.pubmed_id, docs.doi, docs.chembl_id, docs.title, docs.doc_type, docs.authors, docs.abstract, docs.patent_id, docs.ridx, docs.src_id FROM docs WHERE docs.doc_type = 'PUBLICATION' AND (docs.journal IS NULL OR docs.pubmed_id IS NULL AND docs.doi IS NULL AND docs.doc_id NOT IN (64111, 64810, 64842, 64560, 64394));
​
--Docs('DATASET') with null title
 SELECT docs.doc_id FROM docs WHERE docs.doc_type = 'DATASET' AND docs.title IS NULL;
​
--Docs with null chembl_id
 SELECT docs.doc_id FROM docs WHERE docs.chembl_id IS NULL;
​
--Docs with no 'DOCUMENT' entry in chembl_id_lookup
 SELECT docs.doc_id FROM docs WHERE docs.chembl_id NOT IN (SELECT chembl_id_lookup.chembl_id FROM chembl_id_lookup WHERE chembl_id_lookup.entity_type = 'DOCUMENT');
​
--Standard types with a trailing space.
 SELECT DISTINCT activities.standard_type AS anon_1, count(*) AS count_1 FROM activities WHERE activities.standard_type LIKE '% ' GROUP BY activities.standard_type;
​
--Standard types with a leading space.
 SELECT DISTINCT activities.standard_type AS anon_1, count(*) AS count_1 FROM activities WHERE activities.standard_type LIKE ' %' GROUP BY activities.standard_type;
​
--Check that PubChem assays only have one result type per assay.
 SELECT alias_1.activities_assay_id, alias_1.num_types_this_assay FROM (SELECT activities.assay_id AS activities_assay_id, count(DISTINCT activities.standard_type) AS num_types_this_assay FROM activities JOIN assays ON activities.assay_id = assays.assay_id WHERE assays.src_id = 7 GROUP BY activities.assay_id) AS alias_1 WHERE alias_1.num_types_this_assay > 1;
​
--Duplicated bio_component_sequences.
 SELECT bio_component_sequences.component_id, bio_component_sequences.description, bio_component_sequences.sequence_md5sum FROM bio_component_sequences WHERE bio_component_sequences.sequence_md5sum IN (SELECT bio_component_sequences.sequence_md5sum FROM bio_component_sequences GROUP BY bio_component_sequences.sequence_md5sum HAVING count(*) > 1) ORDER BY bio_component_sequences.sequence_md5sum;
​
--Activities potential duplicate can only be 0 or 1, checking for null
 SELECT activities.activity_id, activities.assay_id, activities.doc_id, activities.record_id, activities.molregno, activities.standard_relation, activities.standard_value, activities.standard_units, activities.standard_flag, activities.standard_type, activities.activity_comment, activities.data_validity_comment, activities.potential_duplicate, activities.pchembl_value, activities.bao_endpoint, activities.uo_units, activities.qudt_units, activities.toid, activities.upper_value, activities.standard_upper_value, activities.src_id, activities.type, activities.relation, activities.value, activities.units, activities.text_value, activities.standard_text_value FROM activities WHERE activities.potential_duplicate IS NULL;
​
--Activities potential duplicate can only be 0 or 1, checking for null
 SELECT activities.activity_id, activities.assay_id, activities.doc_id, activities.record_id, activities.molregno, activities.standard_relation, activities.standard_value, activities.standard_units, activities.standard_flag, activities.standard_type, activities.activity_comment, activities.data_validity_comment, activities.potential_duplicate, activities.pchembl_value, activities.bao_endpoint, activities.uo_units, activities.qudt_units, activities.toid, activities.upper_value, activities.standard_upper_value, activities.src_id, activities.type, activities.relation, activities.value, activities.units, activities.text_value, activities.standard_text_value FROM activities WHERE activities.activity_id = 512165 AND activities.potential_duplicate = 0;
​
--Activities potential duplicate can only be 0 or 1, checking for null
 SELECT activities.activity_id, activities.assay_id, activities.doc_id, activities.record_id, activities.molregno, activities.standard_relation, activities.standard_value, activities.standard_units, activities.standard_flag, activities.standard_type, activities.activity_comment, activities.data_validity_comment, activities.potential_duplicate, activities.pchembl_value, activities.bao_endpoint, activities.uo_units, activities.qudt_units, activities.toid, activities.upper_value, activities.standard_upper_value, activities.src_id, activities.type, activities.relation, activities.value, activities.units, activities.text_value, activities.standard_text_value FROM activities WHERE activities.activity_id = 841014 AND activities.potential_duplicate = 1;
​
--Long runnig query
select t.chembl_id, max(pchembl_value) from activities act, assays a, target_dictionary t where act.assay_id = a.assay_id and a.tid = t.tid and act.pchembl_value is not null and act.standard_type = 'Ki' group by t.chembl_id order by max(pchembl_value) desc;
