#!/bin/bash

if [ -z $1 ]; then echo "ERROR: please specify the path as the first argument"; exit; else echo "USING PATH: '$1'"; fi

export TEST_DIR=$1

if [ -d ${TEST_DIR} ]
then

  #To test random write IOPS, run:
  fio -direct=1 -iodepth=128 -rw=randwrite -ioengine=libaio -bs=4k -size=10G -numjobs=1 -runtime=600 -group_reporting -filename=${TEST_DIR}/fiotest.txt -name=Rand_Write_IOPS_Test > ${TEST_DIR}/fio_rand_w_iops.stdout

  #To test random read IOPS, run:
  fio -direct=1 -iodepth=128 -rw=randread -ioengine=libaio -bs=4k -size=10G -numjobs=1 -runtime=600 -group_reporting -filename=${TEST_DIR}/fiotest.txt -name=Rand_Read_IOPS_Test > ${TEST_DIR}/fio_rand_r_iops.stdout

  #To test write throughput, run:
  fio -direct=1 -iodepth=32 -rw=write -ioengine=libaio -bs=1024k -size=10G -numjobs=1 -runtime=600 -group_reporting -filename=${TEST_DIR}/fiotest.txt -name=Write_BandWidth_Test > ${TEST_DIR}/fio_rand_w_trhoughput.stdout

  #To test read throughput, run:
  fio -direct=1 -iodepth=32 -rw=read -ioengine=libaio -bs=1024k -size=10G -numjobs=1 -runtime=600 -group_reporting -filename=${TEST_DIR}/fiotest.txt -name=Read_BandWidth_Test > ${TEST_DIR}/fio_rand_r_trhoughput.stdout

else
    echo "Error: Directory ${TEST_DIR} does not exists."
fi
