FROM debian:jessie-slim

# image parameters
ARG USERNAME=${USERNAME:-fio-user}
ARG GROUP=${GROUP:-fio-group}
ARG UID=${UID:-1234}
ARG GID=${GID:-4321}
ARG WORKDIR=${WORKDIR:-/disk-tester}

# setup user:group and app root directory
RUN groupadd ${GROUP} -g ${GID}
RUN useradd -g ${GROUP} -m ${USERNAME} -u ${UID}
RUN mkdir -p ${WORKDIR}
RUN chown -R ${USERNAME}:${GROUP} ${WORKDIR}
WORKDIR ${WORKDIR}

# required tools
RUN apt-get update
RUN apt-get install -y fio
RUN apt-get install -y wget
RUN apt-get install -y sqlite3

# copy tests
COPY fio-tests.sh .
COPY chembl-test-queries.sql .
COPY chembl-sql-test.sh .

# set image user
USER ${USERNAME}:${GROUP}